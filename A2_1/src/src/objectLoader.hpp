#ifndef OBJECT_LOADER
#define OBJECT_LOADER

#include "object.hpp"

enum FileExt
{
    OBJ,
};

std::vector <Object> loadObject(std::string fileName, FileExt fileExt);
#endif // !OBJECT_LOADER

