#ifndef windows_glfw
#define windows_glfw

#include "gl_common.hpp"

namespace imt2531
{
	struct Interface
	{
		static void * App; // <-- an unknown application to be defined later
	};

	void * Interface::App;

	class  Window
	{
	public:
		GLFWwindow * window;
		Interface interface;
		int mWidth, mHeight;

		int width() { return mWidth; }
		int height() { return mHeight; }
		float ratio() { return (float)mWidth / mHeight; }

		Window() {}

		//Create a Window Context
		template<class APPLICATION>
		void create(APPLICATION * app, GLFWkeyfun fun,  int width, int height, const char * name = "Pacman3D")
		{

			if (!glfwInit())
			{
				fprintf(stderr, "Failed to initialize GLFW\n");
				getchar();
				exit(-1);
			}
			glfwWindowHint(GLFW_SAMPLES, 4);
			glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
			glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

            mWidth = width;
            mHeight = height;
			window = glfwCreateWindow(width, height, "Pacman3D", NULL, NULL);

			if (window == NULL)
			{
				fprintf(stderr, "Failed to open GLFW window.\n");
				getchar();
				glfwTerminate();
				exit(-1);
			}

			glfwMakeContextCurrent(window);

			glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
			glfwSetKeyCallback(window, fun);      // general keyboard input
			glfwSwapInterval(1);
		}

		//Get the Current framebuffer Size in pixels and Set the Viewport to it    
		void setViewport()
		{
			glfwGetFramebufferSize(window, &mWidth, &mHeight);
			glViewport(0, 0, mWidth, mHeight);
		}

		//Check whether window should close
		bool shouldClose()
		{
			return glfwWindowShouldClose(window);
		}

		//Swap front and back buffers
		void swapBuffers()
		{
			glfwSwapBuffers(window);
		}

		//Destroy the window
		void destroy()
		{
			glfwDestroyWindow(window);
		}

		~Window()
		{
			destroy();
		}
	};
}
#endif