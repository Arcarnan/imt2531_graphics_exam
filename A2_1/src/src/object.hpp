#ifndef OBJECT
#define OBJECT

#include <vector>
#include <iostream>
#include <string>
#include "GL/glew.h"

class Object 
{
public:
    std::vector <GLfloat> vertices;	        //	Vertices
    std::vector <GLfloat> normals;	        //	Normals
    std::vector <GLfloat> texCoord;        //	Texture
    std::vector <GLfloat> verticesIndices;  //  
    std::vector <GLfloat> normalsIndices;   //  
    std::vector <GLfloat> textCoordIndices; //  
    std::string mtllib;				        //
    GLuint texture;
    GLuint shader;

	Object() {}

    Object(std::vector <GLfloat> vert, std::vector <GLfloat> norm, std::vector <GLfloat> text, std::string mtll)
    {
		vertices = vert;	//	Vertices
		normals = norm;		//	Normals
		texCoord = text;	//	Texture
		mtllib = mtll;		//
    }
};
#endif // !OBJECT
