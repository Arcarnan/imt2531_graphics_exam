#ifndef FRUIT_HANDLER
#define FRUIT_HANDLER

#include "Fruit.h"
#include "Model.h"
#include <vector>
class FruitHandler
{
public:
	Fruit fruit;				//	Fruit object

	void Init();				//	Initialize
	void update();				//	Update
	void draw();				//	Draw
	void reset();				//	Reset
	void eat();					//	Eat Fruit
};

#endif // !FRUIT_HANDLER