#ifndef GHOST_HANDLER
#define GHOST_HANDLER

#include "Ghost.h"

class GhostHandler
{
public:
	Ghost ghosts[4];				//	List of Ghosts

	void Init();					//	Initialize
	void update();					//	Update
	void reset();					//	Reset

	void draw();					//	Draws the ghosts
	void move();					//	Moves the ghosts
	void checkState();				//	Checks the state of Ghosts
	void changePhase(int id);		//	Changes between scatter and chase
	void scare(int id);				//	Scares the ghost
	void checkPellets();			//	Counts pellets to check whether Ghosts should be active
};
#endif // !GHOSTHANDLER
