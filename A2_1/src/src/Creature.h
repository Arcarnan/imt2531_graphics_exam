#pragma once

#include "enum.h"
#include "Model.h"

class Creature
{
public:
    int posX;				//	Position on the game grid
    int posY;				//	Position on the game grid
    float moveX;			//	Movement from one tile towards another
    float moveY;			//	Movement from one tile towards another
    float speed = .1f;		//	The movement speed
    Direction dir = None;	//	The direction of the movement

	std::vector<Model> model;	//	List of models in creature

    int spriteX;			//	Sprite index
    int spriteY;			//	Sprite index
    int anim = 0;			//	Animation frame
    float animT = 0;		//	Animation timer

    Creature();						//	Constructor
    Creature(int x, int y, float s);//	Constructor
};