#ifndef PACMAN_HANDLER
#define PACMAN_HANDLER


#include "Pacman.h"

class PacmanHandler
{
public:
	Pacman pacman;					//	Pacman object
	Direction lastCam = None;		//	Previous camera direction
	Direction cam = None;			//	Current camera direction
	Direction lastInput = Up;		//	Previous input direction
	Direction lastTurn = Up;		//	Previoud turn direction
	std::vector<Model> models;		//	List of models
	float animDeathTimer;			//	Death animation timer

	void Init();					//	Initialize
	void Update();					//	Update
	void reset();					//	Reset

	void draw();					//	Draw Pacman
	void move();					//	Move Pacman
	void newDirection();			//	Set new direction
	void kill();					//	Kill Pacman
	void die();						//	Start death animation
	void input(Direction dir);		//	Set input relative to camera
	glm::vec2 getPos();				//	Get Position of Pacman
	
};

#endif // PACMAN_HANDLER