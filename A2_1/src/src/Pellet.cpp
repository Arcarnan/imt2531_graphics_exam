#include "Pellet.h"
#include "Globals.h"

Pellet::Pellet()
{
    posX = 0;
    posY = 0;
    active = false;
    power = false;
}

Pellet::Pellet(int x, int y, bool p)
{
    posX = x;
    posY = y;
    active = true;
    power = p;
}
