#version 330

uniform sampler2D sampler;
   
in vec2 texCoord;  
in float diffuse; 
out vec4 outColor;

void main(){
	vec4 tex = texture(sampler, texCoord);
	if(tex.a < 0.1)
		discard;
    outColor = vec4(tex.rgb*diffuse, 1.0);
}
