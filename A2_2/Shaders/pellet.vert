#version 430 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 normals;

out vec4 vertexColor;
out vec2 TexCoord;
out vec3 fragNormal;
out vec3 fragVert;

uniform int[1008] tiles;
uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

void main() {
	fragVert = position;
	fragNormal = normals;
	
	if (tiles[int(round(position.y)) * 28 + int(round(position.x))] == 0) 
	{
		vertexColor = vec4(0.0, 0.0, 0.0, 0.0);
	}
	else 
	{
		vertexColor = vec4(1.0, 1.0, 1.0, 1.0);
	}

	TexCoord = texCoord;
	gl_Position = projection * view * model * vec4(position, 1.0);
}