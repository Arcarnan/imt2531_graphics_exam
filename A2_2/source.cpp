#include "Init.h"
#include "logger.h"
#include "TextBox.h"
#include "Enums.h"
#include "TileMap.h"
#include "Pacman.h"
#include "Pellet.h"
#include "Ghost.h"
#include <vector>
#include <time.h>

#undef main

#ifdef _WIN32
#include "Dependencies\glm\glm\gtc\matrix_transform.hpp"
#include "Dependencies\SDL2_ttf-2.0.14\include\SDL_ttf.h"

#else
#include <glm/gtc/matrix_transform.hpp>
#include <SDL2/SDL_ttf.h>
#endif

#define AUDIO_IMPLEMENTATION
#include "Audio.hpp"

// CONST
const int FPS = 60;

// VARIABLES
GLFWwindow* g_window;

bool pellet_eaten = false;
bool died = false;
double timeElapsed = 0;

int g_score = 0;
int g_lives = 3;
float random_x = 0.0f;
float random_y = 0.0f;
float random_z = 0.0f;

glm::mat4 g_projection;
glm::mat4 g_view;
glm::mat4 rotation;
glm::vec3 camPos;

TextBox box;

// EXTERN VARIABLES
extern Material g_materials[];
extern Mesh g_meshes[];
extern Buffer g_buffers[];

extern Shader g_shaders[];
extern Texture g_textures[];

extern int SCREEN_WIDTH = 800;
extern int SCREEN_HEIGHT = 600;
extern int pellets_left;

extern int tiles[36][28];

extern TileMap map;
extern Pellet pellets;
extern Pacman pacman;
extern Ghost ghost1;
extern Ghost ghost2;
extern Ghost ghost3;
extern Ghost ghost4;

// FUNCTION DECLARATION
void processInput(GLFWwindow* window, char& direction);
void mainMenu();
int pauseMenu();
void runGame();
void deathScreen();
void victoryScreen();

int main()
{
    std::srand(time(nullptr));                      // Randomizes seed
    if (!initWindow()) return -1;                   // Initializes the main window
    if (!Audio::InitAudio()) return -1;             // Initializes the audio
    if (TTF_Init() < 0)                             // Initiliazes TTF for textbox
    {
        std::string str = "Failed TTF";
        LOG_DEBUG("Logging debug: %s Init", str.c_str());
    }

    // Default projection and view matrix
    g_projection = glm::perspective(glm::radians(45.0f), (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 0.1f, 100.0f);
    g_view = glm::translate(g_view, glm::vec3(-14, -18, -25.0f));
    camPos = glm::vec3(14.0f, 18.0f, 50.0f);
    rotation = glm::translate(rotation, glm::vec3(-14, -18, -25.0f));

    // Initializes the different components
    initMaterials();
    initMeshes();
    initEntities();
    //loadSound();

    // Initializes time
    double currentFrame = glfwGetTime();
    double lastFrame = currentFrame;
    double lastFrame2 = currentFrame;
    double lastFrame3 = currentFrame;

    // Starts the game
    mainMenu();

    // Cleanup
    Audio::QuitAudio();
    glfwTerminate();
    return 0;
}

// Only used in game loop
void processInput(GLFWwindow* window, char& direction)
{
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        direction = 'u';
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        direction = 'l';
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        direction = 'd';
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        direction = 'r';
}

void mainMenu()
{
    Audio::PlayBGM("bgm3", -1);

    // Creates a textbox
    box = TextBox(&g_meshes[text], &g_buffers[text], &g_materials[text]);

    // Sets up values used for keypress
    int oldUpKey = 0;
    int oldDownKey = 0;
    int oldReturnKey = 0;

    int newUpKey = GLFW_RELEASE;
    int newDownKey = GLFW_RELEASE;
    int newReturnKey = GLFW_RELEASE;

    int choice = 0;

    SDL_Color colorBlue = { 0, 0, 255, 255 };
    SDL_Color colorWhite = { 255, 255, 255, 255 };

    // Hides cursor on main window
    glfwSetInputMode(g_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

    // Main-menu loop
    while(!glfwWindowShouldClose(g_window))
    {   
        // Clear screen
        glClearColor(0.1f, 0.4f, 0.9f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Draw calls, color based on selected choice
        box.draw("Main menu", colorWhite, glm::vec3(0.0f, 4.0f, 0.0f), center);

        box.draw("Play", (choice == 0) ? colorBlue : colorWhite, glm::vec3(0.0f, 1.0f, 0.0f), center);
        box.draw("Highscore", (choice == 1) ? colorBlue : colorWhite, glm::vec3(0.0f, -1.0f, 0.0f), center);
        box.draw("Exit", (choice == 2) ? colorBlue : colorWhite, glm::vec3(0.0f, -3.0f, 0.0f), center);

        // Input checking, using this since glfwWaitEventsTimeout doesn't work with current linux libraries
        oldUpKey = newUpKey;
        oldDownKey = newDownKey;
        oldReturnKey = newReturnKey;

        newUpKey = glfwGetKey(g_window, GLFW_KEY_UP);
        newDownKey = glfwGetKey(g_window, GLFW_KEY_DOWN);
        newReturnKey = glfwGetKey(g_window, GLFW_KEY_ENTER);

        if (glfwGetKey(g_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)    // Exits if escape is pressed
            glfwSetWindowShouldClose(g_window, true);

        // Decrements / increments choice on up / down key respectivly
        if (newUpKey == GLFW_RELEASE && oldUpKey == GLFW_PRESS)
            choice--;
        if (newDownKey == GLFW_RELEASE && oldDownKey == GLFW_PRESS)
            choice++;

        // Call menu/rungame/exit based on user choice
        if (newReturnKey == GLFW_RELEASE && oldReturnKey == GLFW_PRESS)
        {
            switch (choice)
            {
            case 0:
                runGame();
                break;
            case 1:
                break;
            case 2:
                glfwSetWindowShouldClose(g_window, true);
                break;
            }
        }

        // Clamps choice
        if (choice > 2) choice = 2;
        if (choice < 0) choice = 0;

        // Swap buffer and poll events
        glfwSwapBuffers(g_window);
        glfwPollEvents();
    }
}

int pauseMenu()
{
    Audio::PlayBGM("bgm2", -1);
    Audio::ToggleEffectPause();

    bool paused = true;

    // Sets up values used for keypress
    int oldUpKey = 0;
    int oldDownKey = 0;
    int oldReturnKey = 0;

    int newUpKey = GLFW_RELEASE;
    int newDownKey = GLFW_RELEASE;
    int newReturnKey = GLFW_RELEASE;

    int choice = 1;

    SDL_Color colorBlue = { 0, 0, 255, 255 };
    SDL_Color colorWhite = { 255, 255, 255, 255 };

    // Pause-menu loop
    while (paused)
    {
        // Clear buffers
        glClearColor(0.9f, 0.4f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        // Draw calls
        box.draw("PAUSED", colorWhite, glm::vec3(0.0, 4.0f, 0.0f), center);

        box.draw("Resume", (choice == 1) ? colorBlue : colorWhite, glm::vec3(0.0f, 0.0f, 0.0f), center);
        box.draw("Exit", (choice == 2) ? colorBlue : colorWhite, glm::vec3(0.0f, -2.0f, 0.0f), center);

        // Input checking
        oldUpKey = newUpKey;
        oldDownKey = newDownKey;
        oldReturnKey = newReturnKey;

        newUpKey = glfwGetKey(g_window, GLFW_KEY_UP);
        newDownKey = glfwGetKey(g_window, GLFW_KEY_DOWN);
        newReturnKey = glfwGetKey(g_window, GLFW_KEY_ENTER);

        // Decrements / Increments choice on up / down key respectivly
        if (newUpKey == GLFW_RELEASE && oldUpKey == GLFW_PRESS)
            choice--;
        if (newDownKey == GLFW_RELEASE && oldDownKey == GLFW_PRESS)
            choice++;

        // Ends while loop if pressed
        if (newReturnKey == GLFW_RELEASE && oldReturnKey == GLFW_PRESS)
        {
            paused = false;
        }

        // Clamps choice
        if (choice > 2) choice = 2;
        if (choice < 1) choice = 1;

        // Swap buffer and poll events
        glfwSwapBuffers(g_window);
        glfwPollEvents();
    }

    Audio::ToggleEffectPause();

    // Returns if game should be resumed or exited based on int
    return choice;
}

// Main game loop
void runGame()
{
    Audio::PlayBGM("bgm1", -1);

    // Sets up values
    int resume = 1;
    int camMode = 0;

    SDL_Color colorWhite = { 255, 255, 255, 255 };

    int oldCKey = 0;
    int newCKey = GLFW_RELEASE;

    // Starts time counting
    double currentFrame = glfwGetTime();
    double lastFrame = currentFrame;
    double lastFrame2 = currentFrame;
    double lastFrame3 = currentFrame;

    // main loop
    while (resume != 2)
    {
        // Time counting
        currentFrame = glfwGetTime();
        double deltaTime = currentFrame - lastFrame;
        double deltaTime2 = currentFrame - lastFrame2;
        double deltaTime3 = currentFrame - lastFrame3;

        // If not paused
        if (resume == 1)
        {
            // Input
            char dir;
            processInput(g_window, dir);

            oldCKey = newCKey;
            newCKey = glfwGetKey(g_window, GLFW_KEY_C);

            if (glfwGetKey(g_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)    // Pauses the game
                resume = 0;
            if (newCKey == GLFW_RELEASE && oldCKey == GLFW_PRESS)
                camMode++;
            if (camMode > 2) camMode = 0;

            // Updates positions, physics etc
            map.update(deltaTime);
            pacman.update(deltaTime, dir);
            pellets.update(deltaTime);
            ghost1.update(deltaTime);
            ghost2.update(deltaTime);
            ghost3.update(deltaTime);
            ghost4.update(deltaTime);

            // Game logic
            if (pellet_eaten)                   // Adds camerashake while supperbellet buff is active
            {
                random_x = rand() % 50 / 53;
                random_y = rand() % 50 / 74;
                random_z = rand() % 50 / 34;
                timeElapsed += deltaTime;
            }
            else
            {
                random_x = 0.0f;
                random_y = 0.0f;
                random_z = 0.0f;
            }

            if (died)                   // Resets the board when you loose a life
            {
                pacman.transform.position = pacman.startPos;

                ghost1.reset();
                ghost2.reset();
                ghost3.reset();
                ghost4.reset();

                Audio::StopEffects();
                Audio::PlayEffect("death");

                died = false;
            }

            if (timeElapsed > 15)       // Ends superpellet buff after 15 seconds
            {
                timeElapsed = 0;
                pellet_eaten = false;
            }

            // Only draws every frame to display instead of as fast as possible
            if (deltaTime2 > 1.0 / FPS)
            {
                // Updating of view matrix
                // 1: Hovers over pacman
                if (camMode == 0)
                {
                    g_view = glm::translate(glm::mat4(1.0f), glm::vec3(-pacman.transform.position.x - random_x, -pacman.transform.position.y - random_y, pacman.transform.position.z - 25.0f - random_z));
                    camPos = glm::vec3(g_view * glm::vec4(pacman.transform.position.x, pacman.transform.position.y, pacman.transform.position.z + 25.0f, 0.0f));
                }

                // 2: Rotates the camera around the board
                if (camMode == 1)
                {
                    rotation = glm::rotate(rotation, glm::radians(45.f) * (float)deltaTime2, glm::vec3(0.5f, 1.0f, 0.0f));    // Camera rotation around world
                    g_view = rotation;
                    camPos = glm::vec3(rotation * glm::vec4(14.0f, 18.0f, 25.0f, 0.0f));
                }

                // 3: Hovers over center, but tracks pacman
                if (camMode == 2)
                {
                    g_view = glm::lookAt(glm::vec3(14, 18, 25.0f), pacman.transform.position, glm::vec3(0.0f, 1.0f, 0.0f)); // Better version of camera view
                    camPos = glm::vec3(g_view * glm::vec4(14, 18, 25, 0.0f));
                }

                // Debug
                if (deltaTime3 > 1.0)
                {
                    LOG_DEBUG("X: %f", camPos.x);
                    LOG_DEBUG("Y: %f", camPos.y);
                    LOG_DEBUG("Z: %f", camPos.z);
                    lastFrame3 = currentFrame;
                }


                // Rendering
                glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

                // Draw calls
                map.draw();
                pacman.draw();
                ghost1.draw();
                ghost2.draw();
                ghost3.draw();
                ghost4.draw();
                pellets.draw();

                box.draw("Lives: " + std::to_string(g_lives), colorWhite, glm::vec3(-11.f, 7.4f, -5.0f), left);
                box.draw("Score: " + std::to_string(g_score), colorWhite, glm::vec3( 11.f, 7.4f, -5.0f), right);

                lastFrame2 = currentFrame;

                // Event checking, swap buffers
                glfwSwapBuffers(g_window);
                glfwPollEvents();
            }
        }
        // If game is paused
        else if (resume == 0)
        {
            Audio::StopBGM();
            resume = pauseMenu();               // Goto pause menu
            // Resets time values so entities dont tp outside the board
            currentFrame = glfwGetTime();   

            lastFrame = currentFrame;
            lastFrame2 = currentFrame;
            lastFrame3 = currentFrame;

            if (resume == 1) Audio::PlayBGM("bgm1", -1);    // Only plays music if resumed
        }

        // If you've lost all your lives
        if (g_lives == -1)
        {
            Audio::StopEffects();
            deathScreen();
            resume = 2;
        }

        // If you've eaten all the pellets
        if (pellets_left == 0)
        {
            Audio::StopEffects();
            victoryScreen();
            resume = 2;
        }

        // Frame time update
        lastFrame = currentFrame;
    }

    // Reset everything when exiting the game
    Audio::PlayBGM("bgm3", -1);
    Audio::StopEffects();

    pacman.transform.position = pacman.startPos;

    pellets.reset();
    ghost1.reset();
    ghost2.reset();
    ghost3.reset();
    ghost4.reset();

    g_lives = 3;
    g_score = 0;

    pellet_eaten = false;
}

// Game over screen
void deathScreen()
{
    Audio::PlayBGM("bgm4", -1);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // Sets values
    bool wait = true;
    int choice = 0;

    std::string score = std::to_string(g_score);

    SDL_Color colorBlue = { 0, 0, 255, 255 };
    SDL_Color colorWhite = { 255, 255, 255, 255 };

    // Sets values for input checking
    int oldReturnKey = 0;
    int newReturnKey = GLFW_RELEASE;

    // Game over screen loop
    while (wait)
    {
        // Clear buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Draw calls
        box.draw("Game over", colorWhite, glm::vec3(0.0f, 4.0f, 0.0f), center);

        box.draw("Score:", colorWhite, glm::vec3(0.0f, 1.0f, 0.0f), center);
        box.draw(" " + score + " ", colorWhite, glm::vec3(0.0f, 0.0f, 0.0f), center);
        box.draw("Exit", (choice == 0) ? colorBlue : colorWhite, glm::vec3(0.0f, -3.0f, 0.0f), center);

        // Input handling
        oldReturnKey = newReturnKey;
        newReturnKey = glfwGetKey(g_window, GLFW_KEY_ENTER);

        if (newReturnKey == GLFW_RELEASE && oldReturnKey == GLFW_PRESS)
        {
            wait = false;
        }

        // Swap buffers and poll events
        glfwSwapBuffers(g_window);
        glfwPollEvents();
    }
}

// Game complete screen
void victoryScreen()
{
    Audio::PlayBGM("bgm5", -1);
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    // Sets values
    bool wait = true;
    int choice = 0;
    
    std::string score = std::to_string(g_score);

    SDL_Color colorBlue = { 0, 0, 255, 255 };
    SDL_Color colorBlack = { 0, 0, 0, 255 };

    // Sets values for input checking
    int oldReturnKey = 0;
    int newReturnKey = GLFW_RELEASE;

    // Victory screen loop
    while (wait)
    {
        // Clear buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Draw calls
        box.draw("Victory!", colorBlack, glm::vec3(0.0f, 4.0f, 0.0f), center);

        box.draw("Score:", colorBlack, glm::vec3(0.0f, 1.0f, 0.0f), center);
        box.draw(" " + score + " ", colorBlack, glm::vec3(0.0f, 0.0f, 0.0f), center);
        box.draw("Exit", (choice == 0) ? colorBlue : colorBlack, glm::vec3(0.0f, -3.0f, 0.0f), center);

        // Input handling
        oldReturnKey = newReturnKey;
        newReturnKey = glfwGetKey(g_window, GLFW_KEY_ENTER);

        if (newReturnKey == GLFW_RELEASE && oldReturnKey == GLFW_PRESS)
        {
            wait = false;
        }

        // Swap buffers and poll events
        glfwSwapBuffers(g_window);
        glfwPollEvents();
    }
}
