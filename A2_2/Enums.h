#pragma once

// Enum for game elements, used in arrays
enum Entities
{
    walls,
    consumable,
    player,
    ai,
    text
};

// Enum for textbox allignment
enum Allignment
{
    center,
    left,
    right
};