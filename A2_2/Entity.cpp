#include "Entity.h"

Entity::Entity()
{
    transform.position = glm::vec3(0.0f); // Position 0
    transform.rotation = glm::mat4(1.0f); // Identity matrix
}

void Entity::draw()
{
    // To be implemented by child classes
}

void Entity::update()
{
    // To be implemented by child classes
}