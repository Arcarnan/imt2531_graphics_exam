#pragma once
#include "Entity.h"
#include "Enums.h"

#ifdef _WIN32
#include "Dependencies\SDL2_ttf-2.0.14\include\SDL_ttf.h"
#else
#include <SDL2/SDL_ttf.h>
#endif

class TextBox : public Entity
{
private:
    Mesh * mesh;
    Buffer* buffer;
    Material* material;

    TTF_Font* font;
public:
    TextBox();
    TextBox(Mesh* m, Buffer* buff, Material* mat);

    void draw(std::string text, SDL_Color color, glm::vec3 pos, Allignment allign);
};