#include "Entity.h"
#include "Structs.h"

// Player class
class Pacman : public Entity
{
private:
    Mesh* mesh;
    Buffer* buffer;

public:
    Material* material;

    glm::vec3 startPos;
    glm::vec3 direct;

    Pacman();
    Pacman(Material* mat, Mesh* m, Buffer* buff);

    void draw();
    void update(double delta_time, char& direction);
};
