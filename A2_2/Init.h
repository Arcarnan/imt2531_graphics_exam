﻿#pragma once
#include <iostream>
#include <vector>
#include "Structs.h"

#ifdef _WIN32
#include "Dependencies\glew-2.1.0\include\GL\glew.h"
#include "Dependencies\glfw-3.2.1.bin.WIN32\include\GLFW\glfw3.h"
#else
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#endif

bool initWindow();
void initMaterials();
void initMeshes();
void initEntities();
void loadSound();
void objloadertest(Mesh& mesh, std::string path);

void windowsize_callback(GLFWwindow* window, int width, int height);
