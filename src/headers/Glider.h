#pragma once
#include "GameObject.h"

class Glider : public GameObject
{
private:
    glm::vec3 startPos = glm::vec3(0.0f, 0.0f, 0.0f);

public:
    float speed = 5.0f;
    glm::vec3 rightDirection;
    glm::vec3 lookatDirection;
    glm::mat4 restartRotation;
    glm::vec3 xDirection;

    Glider();

    virtual void update();
    virtual void input(int key, int scancode, int action, int mods);
};
