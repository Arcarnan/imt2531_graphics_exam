#pragma once
#include "GameObject.h"

class Terrain : public GameObject
{
private:
    bool changingSeason = true;

public:
    Terrain();

    virtual void update();
    virtual void input(int key, int scancode, int action, int mods);
};
