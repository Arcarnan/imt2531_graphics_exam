#pragma once
#include "GameObject.h"

class Sun : public GameObject
{
private:

    float sunRotationTime;
    float rotationRadius = 450.0f;
    bool changingDaytime = true;
public:
    Sun();

    virtual void update();
    virtual void input(int key, int scancode, int action, int mods);
};
