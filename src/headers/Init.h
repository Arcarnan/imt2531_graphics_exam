﻿#pragma once
#include <iostream>
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "src/headers/Mesh.hpp"

bool initWindow();
void initTextures();
void initShaders();
void initMeshes();
void initGameObjects();
void loadSound();
void inputHandler (GLFWwindow* window, int key, int scancode, int action, int mods);
void objloadertest(Mesh* mesh, std::string path);   //comment out for Sindre's object loader

void windowsize_callback(GLFWwindow* window, int width, int height);

static void cursorPositionCallback (GLFWwindow* window, double xPos, double yPos);
void cursorPositionCallback(GLFWwindow* window, int entered);
