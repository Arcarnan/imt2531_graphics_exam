#pragma once
#include "GameObject.h"

//--------------------------------------------------------------------
//	TEST CODE, PLEASE DISREGARD
//--------------------------------------------------------------------

// Base class for movable object by input

class Movable : public GameObject
{
public:
    Movable();

    virtual void update();
    virtual void input(int key, int scancode, int action, int mods);
};
