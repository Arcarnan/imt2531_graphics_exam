#pragma once
#include "src/headers/Camera.h"

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;
extern const float PI;

extern double g_deltatime;
extern Camera* mainCamera;
extern GLFWwindow* g_window;
extern float g_season;
extern float g_daytime;
extern bool g_hardline;
extern float g_viewDistance;
extern double g_mousePositionX;
extern double g_mousePositionY;
extern float g_cameraResponsiveness;
extern float g_textSize;
extern float g_seasonChangeDifference;
extern int g_cameraTarget;
extern bool g_fog;
extern float g_seasonChangeSpeed;
