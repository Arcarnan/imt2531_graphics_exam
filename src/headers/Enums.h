#pragma once

// Enum for textbox allignment
enum Allignment
{
    center,
    left,
    right
};

enum CameraMode
{
    freeCam = 0,
    staticCam = 1,
    orbitPositionCam = 2,
    trackingCam = 3,
    orbitTargetCam = 4,
    followCam = 5,
    glider3Cam = 6,
    glider1Cam = 7
};
