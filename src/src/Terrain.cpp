#include "src/headers/Terrain.h"
#include "src/headers/Globals.h"
#include "src/headers/logger.h"


Terrain::Terrain()
{
    //Empty constructor
}

void Terrain::update()

{
    if (changingSeason)
    {
        //Change season if not paused
        g_season +=(float)g_deltatime * g_seasonChangeSpeed;
    }

    if (g_season > 5)
    {
        g_season = 1;
    }
}

void Terrain::input(int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_O && action == GLFW_RELEASE)
    {
        g_hardline = !g_hardline;
    }

    if (key == GLFW_KEY_1 && action == GLFW_RELEASE)
    {
        g_season = 1;
    }

    if (key == GLFW_KEY_2 && action == GLFW_RELEASE)
    {
        g_season = 2;
    }

    if (key == GLFW_KEY_3 && action == GLFW_RELEASE)
    {
        g_season = 3;
    }

    if (key == GLFW_KEY_4 && action == GLFW_RELEASE)
    {
        g_season = 4;
    }

    if (key == GLFW_KEY_5 && action == GLFW_RELEASE)
    {
        changingSeason = !changingSeason;
    }

    if (key == GLFW_KEY_E && action == GLFW_RELEASE)
    {
        g_fog = !g_fog;
    }
}
