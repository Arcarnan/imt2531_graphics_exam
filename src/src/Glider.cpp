#include "src/headers/Glider.h"
#include "src/headers/Globals.h"
#include "src/headers/logger.h"
#include <glm/gtx/rotate_vector.hpp>



Glider::Glider()
{
    upDirection = glm::vec3(0.0, 1.0, 0.0);
    xDirection = glm::vec3(1.0, 0.0, 0.0);
    restartRotation = transform.rotation;
}

void Glider::update()

{
    transform.position += lookatDirection * speed * 0.01f;
    transform.position.y-= 9.81f * g_deltatime * 0.1f;         //  gravity
    //LOG_DEBUG("LOOKAT: X: %f, Y: %f, Z: %f", lookatDirection.x,lookatDirection.y,lookatDirection.z )
}

void Glider::input(int key, int scancode, int action, int mods)
{
    float ROTATE = 3.0f;

    if (key == GLFW_KEY_R && action == GLFW_RELEASE)
    {
        transform.position =  glm::vec3(0, 0, 0);
        transform.rotation = restartRotation;
        lookatDirection = glm::vec3(0.0, 0.0, -1.0);
        upDirection = glm::vec3(0.0, 1.0, 0.0);
    }

    if (key == GLFW_KEY_F && action == GLFW_RELEASE)
    {
        transform.position = glm::vec3(startPos.x + rand()%400 - 200, startPos.y, startPos.z + rand()%400 - 200);   // between 200 and -200, rand() only gives positive values
    }

    if (key == GLFW_KEY_W)
    {
        //transform.rotate(90.0f, glm::vec3(0.0, 0.0, 1.0), g_deltatime);

        lookatDirection = glm::rotate(lookatDirection, -glm::radians(ROTATE), rightDirection);
        lookatDirection = glm::normalize(lookatDirection);
        upDirection = glm::rotate(upDirection, -glm::radians(ROTATE), rightDirection);
        upDirection = glm::normalize(upDirection);
        xDirection = glm::cross(upDirection, lookatDirection);

        transform.rotate(ROTATE, glm::vec3(0, 0, 1), 1);

    }
    if (key == GLFW_KEY_S)
    {
        //transform.rotate(90.0f, glm::vec3(0.0, 0.0, -1.0), g_deltatime);

        lookatDirection = glm::rotate(lookatDirection, glm::radians(ROTATE), rightDirection);
        lookatDirection = glm::normalize(lookatDirection);
        upDirection = glm::rotate(upDirection, glm::radians(ROTATE), rightDirection);
        upDirection = glm::normalize(upDirection);
        xDirection = glm::cross(upDirection, lookatDirection);

        transform.rotate(-ROTATE, glm::vec3(0, 0, 1), 1);
    }

    if (key == GLFW_KEY_A)
    {
        //transform.rotate(90.0f, glm::vec3(1.0, 0.0, 0.0), g_deltatime);

        upDirection = glm::rotate(upDirection, -glm::radians(ROTATE), lookatDirection);
        upDirection = glm::normalize(upDirection);
        rightDirection = glm::rotate(rightDirection, -glm::radians(ROTATE), lookatDirection);
        rightDirection = glm::normalize(rightDirection);
        transform.rotate(ROTATE, glm::vec3(1.0f, 0.0f, 0.0f), 1);
    }
    if (key == GLFW_KEY_D)
    {
        //transform.rotate(90.0f, glm::vec3(-1.0, 0.0, 0.0), g_deltatime);

        upDirection = glm::rotate(upDirection, glm::radians(ROTATE), lookatDirection);
        upDirection = glm::normalize(upDirection);
        rightDirection = glm::rotate(rightDirection, glm::radians(ROTATE), lookatDirection);
        rightDirection = glm::normalize(rightDirection);
        transform.rotate(-ROTATE, glm::vec3(1.0f, 0.0f, 0.0f), 1);
    }

    //  control speed between 5.0f - 25.0f
    if (key == GLFW_KEY_COMMA)
    {
        speed -= 0.1f;
        if (speed < 5.0f)
        {
            speed = 5.0f;
        }
    }
    if (key == GLFW_KEY_PERIOD)
    {
        speed += 0.1f;
        if (speed > 25.0f)
        {
            speed = 25.0f;
        }
    }
}
