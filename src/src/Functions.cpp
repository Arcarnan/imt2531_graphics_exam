#include <vector>
#include <time.h>

#include "src/headers/Functions.h"
#include "src/headers/ObjectHandler.h"
#include "src/headers/Init.h"
#include "src/headers/Globals.h"
#include "src/headers/Camera.h"
#include "src/headers/logger.h"
#include "src/headers/Terrain.h"
#include "src/headers/Sun.h"
#include "src/headers/Glider.h"

#include "src/headers/Movable.h"
#include "src/headers/Textbox2.h"

#define AUDIO_IMPLEMENTATION
#include "src/headers/Audio.hpp"

Light* light;
Terrain* terrain;
Sun* sun;
Glider* glider;
std::vector<GameObject*> clouds;

TextBox seasonText;
TextBox daytimeText;
TextBox gliderSpeedText;
TextBox cameraModeText;
TextBox cameraTargetText;

SDL_Color white = { 255,255,255,255 };

//--------------------------------------------------------------------
//	USED TO INITIALIZE THE GAME AND YOUR GAMEOBJECTS, CLASSES, AND FUNCTIONS
//--------------------------------------------------------------------
void Start()
{
	initTextures();
	initShaders();
	initMeshes();
	initGameObjects();
    //Audio::InitAudio();

	////////////////////////////	LIGHT		////////////////////////////
	light = CreateLight(glm::vec3(1.0f));
	light->color = glm::vec3(0.222f, 0.432f, 0.534f);		//	sunlight color, in order to have it smoother and not completely white


	////////////////////////////	TERRAIN		////////////////////////////
	terrain = new Terrain();
	terrain->material.specularity = 2.0f;
	Mesh* height = new Mesh();					// for tiny obj loader
	objloadertest (height, "../resources/models/height.obj");	//height for preferred map, height50 for fast map (less details), heights4.obj for tall map, height1 for more detailed map (FPS drop)
	Texture* texWhite = LoadTexture("white");
	Shader* standardShader = LoadShader("height");
	InitGameObject(terrain, height, texWhite, standardShader);
	terrain->transform.position = glm::vec3(0.0f, 0.0f, 0.0f);
	terrain->transform.scale = glm::vec3 (1000, 900, 1000);


	////////////////////////////	SUN		////////////////////////////
	sun = new Sun();
	Mesh* sunMesh = new Mesh();
	objloadertest (sunMesh, "../resources/models/watermelon.obj");
	Texture* sunTexture = LoadTexture("white");
	Shader* sunShader = LoadShader ("text");
	InitGameObject (sun, sunMesh, sunTexture, sunShader);
	sun->material.color = glm::vec4(1.0, 1.0, 0.7, 1.0);
	sun->transform.position = glm::vec3(0.0f, -250.0f, 0.0f);
	sun->transform.scale = glm::vec3 (20, 20, 20);


	////////////////////////////	GLIDER		////////////////////////////
	glider = new Glider();
	Mesh* gliderMesh = new Mesh();
	objloadertest (gliderMesh, "../resources/models/glider.obj");
	Texture* gliderTexture = LoadTexture("texture");
	Shader* gliderShader = LoadShader("standard");
	InitGameObject (glider, gliderMesh, gliderTexture, gliderShader );

	// Set values:
    glider->transform.rotate(90, glm::vec3(0, -1, 0), 1);
    glider->transform.translate(glm::vec3(0, 0, 0), 1);
    glider->lookatDirection = glm::vec3(0, 0, -1);
    glider->rightDirection = glm::cross(glider->lookatDirection, glm::vec3(0, (glider->lookatDirection.z >= 0)? -3 : 3, 0));
    glider->restartRotation = glider->transform.rotation;
	glider->material.specularity = 64.0f;


	////////////////////////////	CLOUDS		////////////////////////////
	Mesh* cloudMesh = new Mesh();
	objloadertest (cloudMesh, "../resources/models/cloud5.obj");
	Texture* cloudTexture = LoadTexture("white");
	Shader* cloudShader = LoadShader("text");

	for (int i = 0; i < 6; i++)
	{
		//srand (time(NULL));
		GameObject* cloud = new GameObject();
		InitGameObject (cloud, cloudMesh, cloudTexture, cloudShader);
		cloud->transform.scale = glm::vec3 ((rand() % 5)+4, (rand() % 5)+4, (rand() % 7)+7);
		cloud->material.color = glm::vec4(0.75, 0.75, 0.75, 1.0);
		cloud->transform.position = glm::vec3((rand() % 500) - 250, (rand() % 100) + 50, (rand() % 500) - 250);
		cloud->upDirection = glm::vec3(0.0, ((rand()%2) - 1), 0.0);
		clouds.push_back(cloud);
	}


	////////////////////////////	MAIN CAMERA		////////////////////////////
	//	position, view direction, up direction( portrait 0 0 1, landscape 1 0 0), camera mode
	mainCamera = new Camera(glm::vec3(0, 0, 0), glm::vec3(0, -20, -1), glm::vec3(0, 1, 0), followCam);

	InitGameObject(mainCamera, nullptr, nullptr, nullptr);
	mainCamera->setFollowDistance(glm::vec3(120, 120, 80));
	mainCamera->transform.position = glm::vec3(10.0f, 100.0f, 10.0f);
	//mainCamera->setUpDirection(glm::vec3(0.0f, 1.0f, 0.0f));	//	for correct rotating camera
	//mainCamera->transform.rotate(90, glm::vec3(0.0f, -50.0f, 0.0f), 1);
	mainCamera->setLookatPosition (glm::vec3(0.0f, 0.0f, 0.0f));


	////////////////////////////	TEXTBOX		////////////////////////////
	seasonText.initBox();
	daytimeText.initBox();
	gliderSpeedText.initBox();
	cameraModeText.initBox();
	cameraTargetText.initBox();


	////////////////////////////	AUDIO		////////////////////////////

	//Audio::AddBGM("../resources/sound/pacman_jazz.wav", "bgm", false);
	//Audio::PlayBGM("bgm", -1);


	//--------------------------------------------------------------------
	//	TO CHANGE OBJECT LOADER
	//--------------------------------------------------------------------
	///////////////////////////////////////////////////////////////////////
	//Mesh* height = LoadObject("height");		//for Sindre's object loader
	///////////////////////////////////////////////////////////////////////
	//Mesh* height = new Mesh();					// for tiny obj loader
	//objloadertest (height, "../resources/models/height50.obj");
	///////////////////////////////////////////////////////////////////////
}

//--------------------------------------------------------------------
//	USED TO UPDATE THE GAME, OTHERWISE THE SAME AS START
//--------------------------------------------------------------------
void Update()
{
	//	bind light source to sun position
	lights[0]->transform.position = sun->transform.position;

	//	set light intensity based on time of day
	float value = (500 + sun->transform.position.y) / 1000;
	lights[0]->color = glm::vec3(value, value, value);

	//	rotate clouds
	for (auto cloud : clouds)
		cloud->transform.position = glm::rotate(glm::mat4(1.0f), glm::radians(10.0f) * (float)g_deltatime, glm::vec3(0.0, 1.0, 0.0))*glm::vec4(cloud->transform.position, 1.0f);

	//	update textbox values
	updateSeasonText();
	updateDaytimeText();
	updateGliderSpeed();
	updateCameraModeText();

	//	update camera target (and camera target textbox)
	updateCameraTarget();
}

void updateSeasonText()
{
	std::string season = "Season";
	//	changes season to start of season, does not work well with the paused season change
	/*switch ((int)g_season)
	{
		case 1:
			season="Summer";
			break;

		case 2:
			season="Autumn";
			break;

		case 3:
			season="Winter";
			break;

		case 4:
			season="Spring";
			break;

		default:
			season="Season";
			break;
	}*/

	//	changes season to middle of season, works better with the paused season change
	if (g_season > 1.5f && g_season < 2.5f)
	{
		season="Summer";
	}
	if (g_season > 2.5f && g_season < 3.5f)
	{
		season="Autumn";
	}
	if (g_season > 3.5f && g_season < 4.5f)
	{
		season="Winter";
	}
	if (g_season > 4.5f || g_season < 1.5f)
	{
		season="Spring";
	}

	seasonText.draw(season, white, glm::vec3(-6.0f, 5.8f, 0.0f), left);
}


void updateDaytimeText()
{
	std::string daytime = "Daytime";
	switch ((int)g_daytime)
	{
		case 1:
			daytime="Day";
			break;

		case 2:
			daytime="Sunset";
			break;

		case 3:
			daytime="Night";
			break;

		case 4:
			daytime="Sunrise";
			break;

		default:
			daytime="Daytime";
			break;
	}
	//daytimeText.transform.scale = glm::vec3 (-50.0, -50.0, -50.0);
	daytimeText.draw(daytime, white, glm::vec3(6.0f, 5.8f, 0.0f), right);
}

void updateGliderSpeed()
{
	std::string gliderSpeed = "Speed: " + std::to_string(glider->speed);
	gliderSpeedText.draw(gliderSpeed, white, glm::vec3(-6.0f, -5.0f, 0.0f), left);
}

void updateCameraModeText()
{
	std::string cameraMode = "Cam: " + std::to_string(mainCamera->mode);
	switch (mainCamera->mode)
	{
		case 0:
			cameraMode="Cam: Free";
			break;

		case 1:
			cameraMode="Cam: Static";
			break;

		case 2:
			cameraMode="Cam: Orbit position";
			break;

		case 3:
			cameraMode="Cam: Tracking";
			break;

		case 4:
			cameraMode="Cam: Orbit target";
			break;

		case 5:
			cameraMode="Cam: Follow";
			break;

		case 6:
			cameraMode="Cam: Third person";
			break;

		case 7:
			cameraMode="Cam: First person";
			break;

		default:
			cameraMode="Cam: Unknown";
			break;
	}
	cameraModeText.draw(cameraMode, white, glm::vec3(-6.0f, -5.5f, 0.0f), left);
}

void updateCameraTarget()
{
	std::string cameraTarget = "Target: " + std::to_string(g_cameraTarget);
	switch (g_cameraTarget)
	{
		case 0:
			mainCamera->setTarget(glider);
			cameraTarget = "Target: Glider";
			break;

		case 1:
			mainCamera->setTarget(sun);
			cameraTarget = "Target: Sun";
			break;

		case 2:
			mainCamera->setTarget(terrain);
			cameraTarget = "Target: Terrain";
			break;

		case 3:
			mainCamera->setTarget(clouds[0]);
			cameraTarget = "Target: Cloud";
			break;

		default:
			mainCamera->setTarget(glider);
			cameraTarget = "Target: Glider";
			break;
	}
	cameraTargetText.draw(cameraTarget, white, glm::vec3(-6.0f, -6.0f, 0.0f), left);
}
