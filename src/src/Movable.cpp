#pragma once
#include "src/headers/Movable.h"
#include "src/headers/Globals.h"
#include "src/headers/ObjectHandler.h"

//--------------------------------------------------------------------
//	TEST CODE, PLEASE DISREGARD
//--------------------------------------------------------------------

Movable::Movable()
{
    //  Default constructor
}

void Movable::update()
{

}

void Movable::input(int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_SPACE)
	{
		if (mesh != meshes[0])
			mesh = meshes[0];
		else
			mesh = meshes[1];
	}

    if ( key == GLFW_KEY_UP)
    {
        //transform.position += glm::vec3(0.0f, 1.0f, 0.0f) * (float)g_deltatime;
        transform.position.y += 10.0f * (float)g_deltatime;
    }
    if ( key == GLFW_KEY_DOWN)
    {
        //transform.position += glm::vec3(0.0f, 1.0f, 0.0f) * (float)g_deltatime;
        transform.position.y -= 10.0f * (float)g_deltatime;
    }
    if (key == GLFW_KEY_LEFT)
    {
        //transform.position += glm::vec3(0.0f, 1.0f, 0.0f) * (float)g_deltatime;
        transform.position.x -= 10.0f * (float)g_deltatime;
    }
    if (key == GLFW_KEY_RIGHT)
    {
        //transform.position += glm::vec3(0.0f, 1.0f, 0.0f) * (float)g_deltatime;
        transform.position.x += 10.0f * (float)g_deltatime;
    }
    if (key == GLFW_KEY_P)
    {
        transform.rotation = glm::rotate(transform.rotation, float(glm::radians(180.0f) * g_deltatime), glm::vec3(0, 1, 0));
    }
}
