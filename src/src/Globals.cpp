#include "src/headers/Globals.h"

int SCREEN_WIDTH = 1000;                    //  width of the window
int SCREEN_HEIGHT = 1000;                   //  height of the window
const float PI = 3.1415;                    //  global const for PI value

double g_deltatime;                         //  global deltatime
Camera* mainCamera;                         //  global main camera
GLFWwindow* g_window;                       //  global game window
float g_season = 1.0f;                      //  keeps track of what season it is
float g_seasonChangeDifference = 40.0f;     //  affects how much seasons change
float g_daytime;                            //  keeps track of the time of day
bool g_hardline;                            //  bool to turn off/on hard line/& interpolation
float g_viewDistance = 5000.0f;             //  affects the rendering distance
double g_mousePositionX;                    //  tracks mouse position on screen X coordinate
double g_mousePositionY;                    //  tracks mouse position on screen Y coordinate
float g_cameraResponsiveness = 0.4f;        //  affects the responsiveness of camera movement
float g_textSize = 0.15f;                   //  affects text size (and by effect position)
int g_cameraTarget;                         //  sets camera target from list of game objects
bool g_fog;                                 //  turn fog on/off
float g_seasonChangeSpeed = 0.2f;           //  how fast the seasons change
