#include "src/headers/Camera.h"
#include "src/headers/Globals.h"
#include "src/headers/logger.h"
#include <GLFW/glfw3.h>
#include <algorithm>

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;

Camera::Camera(glm::vec3 pos, glm::vec3 viewDir, glm::vec3 upDir, CameraMode newMode)
{
    transform.position = pos;
    lookatDirection = viewDir;
    upDirection = upDir;
    mode = newMode;

    rotationAngle = 45.0f;
    rotationSpeed = 1.0f;
    rotationDirection = glm::vec3(0, 1, 0);
    xDirection = glm::cross(upDirection, lookatDirection);
    lookatPosition = glm::vec3(0, 0, 0);
    followDistance = glm::vec3(0, 0, 1);
    transform.rotation = glm::mat4(1.0f);

    view = glm::mat4(1.0f);
    projection = glm::perspective(glm::radians(FOV), float(SCREEN_WIDTH) / float(SCREEN_HEIGHT), 0.1f, g_viewDistance);
}

void Camera::update()
{
    if (mode == freeCam)
    {
        double xpos, ypos;

        glfwGetCursorPos(g_window, &xpos, &ypos);

        float xdiff = abs(xpos - (SCREEN_WIDTH / 2.f)) / 4.f;
        float ydiff = abs(ypos - (SCREEN_HEIGHT / 2.f)) / 4.f;

        if (xpos < SCREEN_WIDTH / 2.0f)
        {
            glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), glm::radians(rotationAngle) * float(g_deltatime) * xdiff, glm::vec3(0, 1, 0));
            lookatDirection = rotation * glm::vec4(lookatDirection, 1.0f);
            upDirection = rotation * glm::vec4(upDirection, 1.0f);
            xDirection = glm::cross(lookatDirection, upDirection);
        }
        if (xpos > SCREEN_WIDTH / 2.0f)
        {
            glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), glm::radians(rotationAngle) * float(g_deltatime) * xdiff, glm::vec3(0, -1, 0));
            lookatDirection = rotation * glm::vec4(lookatDirection, 1.0f);
            upDirection = rotation * glm::vec4(upDirection, 1.0f);
            xDirection = glm::cross(lookatDirection, upDirection);
        }

        if (ypos > SCREEN_HEIGHT / 2.0f)
        {
            transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * ydiff), -xDirection);
            lookatDirection = transform.rotation * glm::vec4(lookatDirection, 1.0);
            upDirection = glm::cross(xDirection, lookatDirection);
        }
        if (ypos < SCREEN_HEIGHT / 2.0f)
        {
            transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * ydiff), xDirection);
            lookatDirection = transform.rotation * glm::vec4(lookatDirection, 1.0);
            upDirection = glm::cross(xDirection, lookatDirection);
    }
}

    switch (mode)
    {
    case freeCam:
        view = glm::lookAt(transform.position, transform.position + lookatDirection, upDirection);
        view = view * transform.rotation;
        break;

    case staticCam:
        view = glm::lookAt(transform.position, lookatPosition, upDirection);
        break;

    case orbitPositionCam:
        transform.rotation = glm::rotate(glm::mat4(1.0f), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed), rotationDirection);

        followDistance = transform.rotation * glm::vec4(followDistance, 1.0);
        transform.position = lookatPosition + followDistance;
        view = glm::lookAt(transform.position, lookatPosition, upDirection);
        break;

    case trackingCam:
        view = glm::lookAt(transform.position, target->transform.position, upDirection);
        break;

    case orbitTargetCam:
        // Orbit gameobject
        transform.rotation = glm::rotate(glm::mat4(1.0f), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed), rotationDirection);

        followDistance = transform.rotation * glm::vec4(followDistance, 1.0);
        transform.position = target->transform.position + followDistance;
        view = glm::lookAt(transform.position, target->transform.position, upDirection);
        break;

    case followCam:
        transform.position = target->transform.position + glm::vec3(target->transform.rotation * glm::vec4(followDistance, 1.0f));
        view = glm::lookAt(transform.position, target->transform.position, upDirection);
        break;

    case glider3Cam:
        transform.position = target->transform.position + glm::vec3(target->transform.rotation * glm::vec4(glm::vec3(10.0, 3.0, 0.0), 1.0));
        view = glm::lookAt(transform.position,
            target->transform.position,// + lookatDirection + glm::vec3(target->transform.rotation * glm::vec4(0.0, 1.0, 0.0, 1.0)),
            target->upDirection);
        break;

    case glider1Cam:
        transform.position = target->transform.position + glm::vec3(target->transform.rotation * glm::vec4(glm::vec3(-2.4, -0.3, 0.0), 1.0));
        view = glm::lookAt(transform.position,
            target->transform.position + (lookatDirection * 5.f),// + glm::vec3(target->transform.rotation * glm::vec4(0.0, 1.0, 0.0, 1.0)),
            target->upDirection);
        break;
    }

    glfwSetCursorPos(g_window, SCREEN_WIDTH / 2.0, SCREEN_HEIGHT / 2.0);
}

void Camera::input(int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_C && action == GLFW_PRESS)
    {
        mode = static_cast<CameraMode>(int(mode) + 1);
        if (int(mode) > 5)
        {
            mode = freeCam;
            //transform.position = glm::vec3(0, 0, 5);
            transform.rotation = glm::mat4(1.0f);
        }
        //LOG_DEBUG("Cam mode: %d", int(mode));

        lookatDirection = glm::vec3(0, 0, -1.0f);
        transform.rotation = glm::mat4(1.0f);
        upDirection = glm::vec3(0, 1, 0);
        xDirection = glm::cross(lookatDirection, upDirection);
    }

    if (key == GLFW_KEY_T && action == GLFW_PRESS)
    {
        g_cameraTarget += 1;
        if (g_cameraTarget > 3)
        {
            g_cameraTarget = 0;
        }
        //LOG_DEBUG("camTarget: %d", g_cameraTarget);
    }

    if (key == GLFW_KEY_SLASH && action == GLFW_PRESS)
    {
        if (mode == glider1Cam)
        {
            mode = freeCam;
            lookatDirection = glm::vec3(0, 0, -1.0f);
            transform.rotation = glm::mat4(1.0f);
            upDirection = glm::vec3(0, 1, 0);
            xDirection = glm::cross(lookatDirection, upDirection);
        }
        else if (mode == glider3Cam)
        {
            mode = glider1Cam;
        }
        else
        {
            mode = glider3Cam;
        }

    }

    if (mode == freeCam)
    {
        glm::vec3 newDir = glm::vec3(0);

        //  Y axis indrease
        if (key == GLFW_KEY_Y)
        {
            transform.position -= glm::cross (lookatDirection,  glm::vec3(1.0, 0.0, 0.0))* g_cameraResponsiveness;
        }

        //  Y axis decrease
        if (key == GLFW_KEY_H)
        {
            transform.position -= glm::cross (lookatDirection,  glm::vec3(-1.0, 0.0, 0.0)) * g_cameraResponsiveness;
        }

        //  Z axis indrease
        if (key == GLFW_KEY_I)
        {
            transform.position += lookatDirection * g_cameraResponsiveness;
        }

        //  Z axis decrease
        if (key == GLFW_KEY_K)
        {
            transform.position -= lookatDirection * g_cameraResponsiveness;
        }

        //  X axis increase
        if (key == GLFW_KEY_L)
        {
            transform.position -= glm::cross (lookatDirection,  glm::vec3(0.0, -1.0, 0.0)) * g_cameraResponsiveness;
        }

        //  X axis decrease
        if (key == GLFW_KEY_J)
        {
            transform.position -= glm::cross (lookatDirection, glm::vec3(0.0, 1.0, 0.0)) * g_cameraResponsiveness;
        }

        //  Zoom in, increase field of view
        if (key == GLFW_KEY_N)
        {
            FOV += 2.0f;
            if (FOV > 150.0f)
            {
                FOV = 150.0f;
            }
            projection = glm::perspective(glm::radians(FOV), float(SCREEN_WIDTH) / float(SCREEN_HEIGHT), 0.1f, g_viewDistance);
        }

        //  Zoom out, decrease field of view
        if (key == GLFW_KEY_M)
        {
                FOV -= 2.0f;
                if (FOV < 5.0f)
                {
                    FOV = 5.0f;
                }
                projection = glm::perspective(glm::radians(FOV), float(SCREEN_WIDTH) / float(SCREEN_HEIGHT), 0.1f, g_viewDistance);
        }

        //  reset field of view
        if (key == GLFW_KEY_B)
        {
            FOV = 45.0f;
            projection = glm::perspective(glm::radians(FOV), float(SCREEN_WIDTH) / float(SCREEN_HEIGHT), 0.1f, g_viewDistance);
        }
    }
}

void Camera::mouseInput(double xPos, double yPos)
{
    if (mode == freeCam)
    {
        //  in case of mouse button input
    }
}

void Camera::rotate(glm::vec3 rot)
{
    transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed * 4), rot);
    lookatDirection = transform.rotation * glm::vec4(lookatDirection, 1.0);
}

void Camera::tilt(glm::vec3 dir)
{
    transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed * 4), dir);
    upDirection = transform.rotation * glm::vec4(upDirection, 1.0);
}

void Camera::setCamMode(CameraMode newMode)
{
    mode = newMode;
}

void Camera::setFollowDistance(glm::vec3 newDistance)
{
    followDistance = newDistance;
}

void Camera::setUpDirection(glm::vec3 newDirection)
{
    upDirection = newDirection;
}

void Camera::setLookatDirection(glm::vec3 newDirection)
{
    lookatDirection = newDirection;
}

void Camera::setLookatPosition(glm::vec3 newPosition)
{
    lookatPosition = newPosition;
}

void Camera::setTarget(GameObject* newTarget)
{
    target = newTarget;
}

void Camera::setRotationDirection(glm::vec3 newDirection)
{
    rotationDirection = newDirection;
}

void Camera::setRotationAngle(float angle)
{
    rotationAngle = angle;
}

void Camera::setRotationSpeed(float speed)
{
    rotationSpeed = speed;
}

void Camera::updateProjection(int width, int height)
{
    projection = glm::perspective(glm::radians(45.0f), float(width) / float(height), 0.1f, g_viewDistance);
}

glm::vec3 Camera::getFollowDistance()
{
    return followDistance;
}

glm::vec3 Camera::getUpDirection()
{
    return upDirection;
}

glm::vec3 Camera::getLookatDirection()
{
    return lookatDirection;
}

glm::vec3 Camera::getLookatPosition()
{
    return lookatPosition;
}

glm::mat4 Camera::getProjection()
{
    return projection;
}

glm::mat4 Camera::getView()
{
    return view;
}

GameObject* Camera::getTarget()
{
    return target;
}
