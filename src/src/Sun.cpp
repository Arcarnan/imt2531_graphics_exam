#include "src/headers/Sun.h"
#include "src/headers/Globals.h"
#include "src/headers/logger.h"



Sun::Sun()
{
    //Empty constructor
}

void Sun::update()

{
    if (changingDaytime)
    {
        //change day time when not paused
        g_daytime +=(float)g_deltatime;
        transform.position = glm::vec3(cos(((g_daytime-1)/4)*PI*2)*rotationRadius, 50 + glm::sin(((g_daytime-1)/4)*PI*2)*rotationRadius, 0);
    }

    if (g_daytime > 5)
    {
        g_daytime = 1;
    }
}

void Sun::input(int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_0 && action == GLFW_RELEASE)
    {
        changingDaytime = !changingDaytime;
    }

    if (key == GLFW_KEY_6 && action == GLFW_RELEASE)
    {
        g_daytime = 1;
        //transform.position = glm::vec3(cos(PI/2)*450.0f, 50 + glm::sin(1)*450.0f, 0);       //  full, actuall calculation
        transform.position = glm::vec3(0.0f, 50.0f + rotationRadius, 0);
    }

    if (key == GLFW_KEY_7 && action == GLFW_RELEASE)
    {
        g_daytime = 2;
        //transform.position = glm::vec3(cos(PI)*450.0f, 50 + glm::sin(0)*450.0f, 0);         //  full, actuall calculation
        transform.position = glm::vec3(-rotationRadius, 50.0f, 0);
    }

    if (key == GLFW_KEY_8 && action == GLFW_RELEASE)
    {
        g_daytime = 3;
        //transform.position = glm::vec3(cos(PI/2)*450.0f, 50 + glm::sin(-1)*450.0f, 0);      //  full, actuall calculation
        transform.position = glm::vec3(0.0f, 50.0f - rotationRadius, 0);
    }

    if (key == GLFW_KEY_9 && action == GLFW_RELEASE)
    {
        g_daytime = 4;
        //transform.position = glm::vec3(cos(2*PI)*450.0f, 50 + glm::sin(0)*450.0f, 0);       //  full, actuall calculation
        transform.position = glm::vec3(rotationRadius, 50.0f, 0);
    }
}
